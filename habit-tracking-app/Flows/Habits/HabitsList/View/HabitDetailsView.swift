//
//  HabitDetailsView.swift
//  habit-tracking-app
//
//  Created by Dawid Żubrowski on 27/11/2021.
//

import SwiftUI

struct HabitDetailsView: View {
    var habit: HabitModel
    
    var body: some View {
        ZStack {
            Color.gray
                .ignoresSafeArea()
            HStack {
                VStack {
                    Text(habit.activity)
                    Text(habit.type.rawValue)
                    Text("id: \(habit.id)")
                }
            }
            Spacer()
            Text(String(habit.duration))
        }
    }
}
