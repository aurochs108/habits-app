//
//  ContentView.swift
//  habit-tracking-app
//
//  Created by Dawid Żubrowski on 23/11/2021.
//

import SwiftUI

struct HabitsListView: View {
    @ObservedObject var viewModel = HabitsListViewModel()
    @State var choosedHabitUUID: UUID? = nil
    @State var showingAddHabitSheet = false
    
    var body: some View {
        NavigationView {
            VStack {
                List {
                    ForEach(viewModel.habits, id: \.id) { habit in
                        HabitListRowView(habit: habit)
                            .onTapGesture {
                                choosedHabitUUID = choosedHabitUUID == habit.id ? nil : habit.id
                            }
                            .environmentObject(self.viewModel)
                    }
                }
                buildHabitDetailsView()
            }
            .frame(
                maxWidth: .infinity,
                maxHeight: .infinity
            )
            .navigationTitle("Habits list")
            .toolbar {
                Button("Add") {
                    showingAddHabitSheet = true
                }
            }
            .sheet(isPresented: $showingAddHabitSheet) {
                NavigationView {
                    AddHabitView(showingAddHabitSheet: $showingAddHabitSheet)
                        .environmentObject(self.viewModel)
                }
            }
        }
    }
    
    @ViewBuilder private func buildHabitDetailsView() -> some View {
        if let choosedHabitUUID = choosedHabitUUID,
           let choosenHabit = viewModel.habits.first(where: { $0.id == choosedHabitUUID } ) {
            HabitDetailsView(habit: choosenHabit)
        }
    }
}
