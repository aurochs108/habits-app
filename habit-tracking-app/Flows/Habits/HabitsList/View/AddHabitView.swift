//
//  AddHabitView.swift
//  habit-tracking-app
//
//  Created by Dawid Żubrowski on 27/11/2021.
//

import SwiftUI

struct AddHabitView: View {
    @EnvironmentObject var model: HabitsListViewModel
    @State var activity = ""
    @State var duration = "0.0"
    @State var type: HabitTypeEnum = .sport
    @Binding var showingAddHabitSheet: Bool
    
    var body: some View {
        Form {
            Picker("Select activity type:", selection: $type) {
                ForEach(HabitTypeEnum.allCases, id: \.self) {
                    Text($0.rawValue)
                }
            }
            TextField("Activity name", text: $activity)
            TextField("Duration", text: $duration)
            Button("Add habit") {
                addHabit()
            }
            Button("Cancel") {
                showingAddHabitSheet = false
            }
        }
    }
    
    private func addHabit() {
        showingAddHabitSheet = false
        model.habits.append(.init(type: type, activity: activity, duration: Float(duration)!))
    }
}
