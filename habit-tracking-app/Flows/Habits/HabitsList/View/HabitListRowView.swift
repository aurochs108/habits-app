//
//  HabitListRowView.swift
//  habit-tracking-app
//
//  Created by Dawid Żubrowski on 27/11/2021.
//

import SwiftUI

struct HabitListRowView: View {
    @EnvironmentObject var model: HabitsListViewModel
    var habit: HabitModel
    
    var body: some View {
        HStack {
            VStack {
                Text(habit.activity)
                Text(habit.type.rawValue)
            }
            Spacer()
            Text(String(habit.duration))
            Text("+")
                .onTapGesture {
                    addDuration()
                }
        }
    }
    
    private func addDuration() {
        if let index = model.habits.firstIndex(where: { $0 == habit }) {
            model.habits[index].duration += 1.0
        }
    }
}
