//
//  HabitsListViewModel.swift
//  habit-tracking-app
//
//  Created by Dawid Żubrowski on 24/11/2021.
//

import Foundation

class HabitsListViewModel: ObservableObject {
    @Published var habits: [HabitModel] = []
}
