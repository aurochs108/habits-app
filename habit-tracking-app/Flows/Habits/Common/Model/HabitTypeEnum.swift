//
//  HabitTypeEnum.swift
//  habit-tracking-app
//
//  Created by Dawid Żubrowski on 24/11/2021.
//

import Foundation

enum HabitTypeEnum: String, CaseIterable {
    case sport
    case learn
    case relax
    case business
}
