//
//  HabitModel.swift
//  habit-tracking-app
//
//  Created by Dawid Żubrowski on 24/11/2021.
//

import Foundation

struct HabitModel: Identifiable, Equatable {
    let id = UUID()
    var type: HabitTypeEnum
    var activity: String
    var duration: Float
}
