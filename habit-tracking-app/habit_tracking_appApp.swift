//
//  habit_tracking_appApp.swift
//  habit-tracking-app
//
//  Created by Dawid Żubrowski on 23/11/2021.
//

import SwiftUI

@main
struct habit_tracking_appApp: App {
    var body: some Scene {
        WindowGroup {
            HabitsListView()
        }
    }
}
